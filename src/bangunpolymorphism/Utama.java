/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bangunpolymorphism;
import java.util.Scanner;
/**
 *
 * @author ROG SERIE
 */
public class Utama 
{
  public static void main (String [] args)
    {
        int x;
        
        double a=0,at=0,tg=0,s=0,j=0;
        int pilih = 0;

        
        Scanner s1 = new Scanner(System.in);
        Scanner p = new Scanner(System.in);
        Scanner als = new Scanner(System.in);
        Scanner t = new Scanner(System.in);
        Scanner atp = new Scanner(System.in);
        Scanner jr = new Scanner(System.in);
            
        System.out.print("Masukkan Jumlah bangun : ");
        x = s1.nextInt();
        System.out.println();
        Bangun [] dataBangun = new Bangun[x];
                
        for (int i = 0; i < x; i++)
        { 
            System.out.println("Pilih bangun ke-"+ (i+1));
            System.out.println("1.Segitiga (siku-siku)");
            System.out.println("2.Persegi");
            System.out.println("3.Trapesium Sama Kaki");
            System.out.println("4.Lingkaran");
            System.out.print("Masukkan Pilihan anda :" );
            pilih  = p.nextInt();
            System.out.println();
            
            if (pilih == 1)
            {
                System.out.print("Masukkan panjang alas (cm) : ");
                a = als.nextDouble();
                System.out.print("Masukkan Tinggi (cm) : ");
                tg = t.nextDouble(); 
                dataBangun [i] = new Segitiga(a,tg);
                System.out.println();
            }
            else if(pilih == 2)
            {
                System.out.print("Masukkan panjang sisi (cm) : ");
                s = als.nextDouble();
                dataBangun [i] = new Persegi(s);
                System.out.println();
            }
            else if(pilih == 3)
            {
                System.out.print("Masukkan panjang alas (cm) : ");
                a = als.nextDouble();
                System.out.print("Masukkan panjang Atap (cm) : ");
                at = atp.nextDouble();
                System.out.print("Masukkan Tinggi (cm) : ");
                tg = t.nextDouble(); 
                dataBangun [i] = new Trapesium(a,at,tg);
                System.out.println();
            }
            else if(pilih == 4)
            {
                System.out.print("Masukkan panjang jari-jari (cm) : ");
                j = jr.nextDouble();
                dataBangun [i] = new Lingkaran(j);
                System.out.println(); 
            } 
        }
        System.out.println();
        System.out.println("Luas dan keliling tiap bangun : ");
        System.out.println();
        
        for(int o=0 ;o < x;o++)
        {
            int no = o+1;
            
            if (dataBangun [o] instanceof Segitiga)
            {
               System.out.println(no+". Bangun ke-"+no+"(Segitiga)");
               System.out.println("Panjang Alas : " + a +" cm"  );
               System.out.println("Panjang Atap : " + at +" cm"  );
               cetakLuasKeliling((Segitiga)dataBangun[o]);
               System.out.println("================================");
            }
            else if(dataBangun [o] instanceof Persegi)
            {
               System.out.println(no+". Bangun ke-"+no+"(Persegi)");
               System.out.println("Panjang Sisi : " + s +" cm"  );
               cetakLuasKeliling((Persegi)dataBangun[o]); 
               System.out.println("================================");
            }
            else if(dataBangun [o] instanceof Trapesium)
            {
               System.out.println(no+". Bangun ke-"+no+"(Trapesium Sama Kaki)");
               System.out.println("Panjang Alas : " + a +" cm"  );
               System.out.println("Panjang Atap : " + at +" cm"  );
               System.out.println("Panjang Tinggi : " + tg +" cm"  );
               cetakLuasKeliling((Trapesium)dataBangun[o]);
               System.out.println("================================");
            }
            else if(dataBangun [o] instanceof Lingkaran)
            {
               System.out.println(no+". Bangun ke-"+no+"(Lingkaran)");
               System.out.println("Panjang Jari Jari : " + j +" cm" );
               System.out.println("Panjang Jari Diameter : " + (j+j) +" cm" );
               cetakLuasKeliling((Lingkaran)dataBangun[o]);
               System.out.println("================================");
            }
    } 
  }
static void cetakLuasKeliling(ICetak cetak)
{
     cetak.cetakKeliling();
     cetak.cetakLuas();
}

}

