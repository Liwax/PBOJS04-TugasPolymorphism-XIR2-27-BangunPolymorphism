/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bangunpolymorphism;

/**
 *
 * @author ROG SERIE
 */
public class Lingkaran extends Bangun implements ICetak{

     double jari;
    
    public Lingkaran(double jari)
    {
        this.jari = jari;
    }
@Override
   public double luas(){
       double l = Math.PI * jari * jari;
       return l;
   }
   
    @Override
   public double keliling(){
       double kl;
       kl = Math.PI * jari * 2;
       return kl;
   }
   
    @Override
    public void cetakLuas(){
       System.out.println("Luas "+luas()+" cm persegi");
   }
   
    @Override
   public void cetakKeliling(){
       System.out.println("Keliling "+keliling()+" cm"); 
   }

}
