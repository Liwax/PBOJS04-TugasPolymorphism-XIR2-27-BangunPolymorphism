/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bangunpolymorphism;

/**
 *
 * @author ROG SERIE
 */
public class Persegi extends Bangun implements ICetak {
   double sisi;
    
    public Persegi(double s)
    {
        this.sisi = s;
    }
 @Override
   public double luas(){
       double l = Math.pow(sisi, 2);
       return l;
   }
   
    @Override
   public double keliling(){
      double kl = sisi * 4;
      return kl;
   }
   
    @Override
    public void cetakLuas(){
       System.out.println("Luas "+luas()+" cm persegi");
   }
   
    @Override
   public void cetakKeliling(){
       System.out.println("Keliling "+keliling()+" cm"); 
   }
}
