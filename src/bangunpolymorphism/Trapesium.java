/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bangunpolymorphism;

/**
 *
 * @author ROG SERIE
 */
public class Trapesium extends Bangun implements ICetak{
    
    double alas1,atap,tinggi;
    
    public Trapesium(double alas1,double alas2, double tinggi)
    {
        this.alas1 = alas1;
        atap = alas2;
        this.tinggi = tinggi;
    }
@Override
   public double luas(){
       double l = ((alas1+atap) * tinggi) / 2;
       return l;
   }
   
    @Override
   public double keliling(){
       double m,kl,alk;
       alk = (alas1 - atap) / 2;
       m = Math.sqrt(Math.pow(alk, 2) + Math.pow(this.tinggi, 2));
       kl = alas1 + tinggi + atap + (m * 2);
       return kl;
   }
   
    @Override
   public void cetakLuas(){
       System.out.println("Luas "+luas()+" cm persegi");
   }
   
    @Override
   public void cetakKeliling(){
       System.out.println("Keliling "+keliling()+" cm"); 
   }
}
