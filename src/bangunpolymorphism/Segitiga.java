/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bangunpolymorphism;

/**
 *
 * @author ROG SERIE
 */
public class Segitiga extends Bangun implements ICetak 
{
  double alas,tinggi;
    
    public Segitiga(double alas, double tinggi)
    {
        this.alas = alas;
        this.tinggi = tinggi;
    }
 @Override
   public double luas(){
       double l = (this.alas * this.tinggi) / 2;
       return l;
   }
   
    @Override
   public double keliling(){
       double m,kl;
       m = Math.sqrt(Math.pow(this.alas, 2) + Math.pow(this.tinggi, 2));
       kl = this. alas + this.tinggi + m;
       return kl;
   }
   
    @Override
   public void cetakLuas(){
       System.out.println("Luas "+luas()+" cm persegi");
   }
   
    @Override
   public void cetakKeliling(){
       System.out.println("Keliling "+keliling()+" cm"); 
   }  
}
